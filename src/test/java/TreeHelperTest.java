import com.alibaba.fastjson.JSON;
import site.lizhivscaomei.libs.tree.entity.TreeDestNode;
import site.lizhivscaomei.libs.tree.service.TreeHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TreeHelperTest {
    public static void main(String args[]){
        List<Menu> sourceList=new ArrayList<Menu>();
        for(int i=0;i<100;i++){
            Menu menu=new Menu();
            menu.setId(i+"55");
            menu.setParentId("0");
            menu.setName("node:"+i);
            sourceList.add(menu);
            for(int j=0;j<10;j++){
                Menu menu1=new Menu();
                menu1.setId(i+""+j);
                menu1.setParentId(i+"55");
                menu1.setName("node:"+i+j);
                sourceList.add(menu1);

                for(int k=0;k<10;k++){
                    Menu menuk=new Menu();
                    menuk.setId(i+""+j+k);
                    menuk.setParentId(i+""+j);
                    menuk.setName("node:"+i+j+k);
                    sourceList.add(menuk);
                }
            }
        }
        System.out.println(sourceList.size()+"条数据");
        Date begin=new Date();
        List<TreeDestNode<Menu>> res= TreeHelper.convert(sourceList);
        Date end=new Date();
        System.out.println(sourceList.size()+"条数据，耗费时间："+(end.getTime()-begin.getTime())+"毫秒");
        System.out.println(JSON.toJSONString(res));
    }
}
