import site.lizhivscaomei.libs.tree.entity.TreeSourceNode;

public class Menu extends TreeSourceNode {
    private String parentId;
    private String id;
    private String name;

    @Override
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
