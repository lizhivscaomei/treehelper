package site.lizhivscaomei.libs.tree.entity;

/**
 * 转化前的节点
 */
public abstract class TreeSourceNode {
    //    获取节点ID
    public abstract String getId();

    //    获取节点名称
    public abstract String getName();

    //    获取父节点ID
    public abstract String getParentId();

}
