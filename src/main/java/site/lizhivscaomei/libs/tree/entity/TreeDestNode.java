package site.lizhivscaomei.libs.tree.entity;

import java.util.List;

/**
 * 转化后的树形节点
 */
public class TreeDestNode<T extends TreeSourceNode> {
    //唯一标识
    private String id;
    //名称
    private String name;
    //级别
    private int level;
    //是否禁用，默认没禁用
    private boolean disabled = false;
    //子节点
    private List<TreeDestNode<T>> children;
    //元数据
    private T meta;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public List<TreeDestNode<T>> getChildren() {
        return children;
    }

    public void setChildren(List<TreeDestNode<T>> children) {
        this.children = children;
    }

    public T getMeta() {
        return meta;
    }

    public void setMeta(T meta) {
        this.meta = meta;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
